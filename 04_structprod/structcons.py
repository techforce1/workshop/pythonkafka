# structcons
#
# Consume een topic op basis van record based (de)deserialisation
# 
#
#

import faust
import structprod

app = faust.App('structcons', broker='kafka-1:19092')
topic = app.topic('ipflow',value_type=structprod.IPFlow)

@app.agent(topic)
async def ipflow(ipflows):
    async for flow in ipflows:
        print(f'Source IP address: {flow.src_ip}') ## vul hier aan met de overige velden uit het model aangemaakt in structprod.py

if __name__=="__main__":
    app.main()