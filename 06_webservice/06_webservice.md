## Bouw door op de gedefnieerde Table uit 05 en voeg een simpele rest server toe

- check https://faust.readthedocs.io/en/latest/userguide/tasks.html#id4 ivm webviews

- pas table.py zo aan er een simpele key:value JSON onder de URL /totalflows/ vandaan komt.

- bij een goed geconfigureerde webview, start je de worker deze keer *met* web port:

$ python3 ./table.py worker -l debug  

