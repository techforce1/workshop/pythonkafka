# simpledyncons.py:
#
#
# consume stream van fixed topic en print stream op scherm
#
#

import faust

app = faust.App('simpledyncons', broker='kafka-1:19092')
topic = app.topic('dynprod')

@app.agent(topic)
async def process(stream):
    async for value in stream:
        print(value)

if __name__=="__main__":
    app.main()