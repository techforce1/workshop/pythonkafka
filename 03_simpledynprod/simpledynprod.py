"""
Simpele Faust based producer die:
- topic aanmaakt (in voorbeeld dynprod)
- dyanmisch bericht op dat topic produced (als simpele demo van combinatie met standard Python library)
"""
import faust
import random

# maak Faust app aan

app = faust.App('simpledynprod', broker='kafka-1:19092')
topic = app.topic('dynprod')

# registreer een async func (send_message):
# produceer een dynamische string (msg) elke (interval) seconden met een random ID, zie Python docs module random

@app.timer(interval=1.0)
async def send_message(message):
    msg = "ID = "  ## aanvullen met string concat en random module
    await topic.send(value=msg)

if __name__ == '__main__':

    # Start de Faust async eventloop
    app.main()