# COnstrueer een Table (zie Faust docs) om flows te aggregren
# 
#
#

import faust
import structprod

app = faust.App('table', topic_partitions=1, broker='kafka-1:19092')
topic = app.topic('ipflow2',value_type=structprod.IPFlow,partitions=1)
total_flows = app.Table(#hier table met 1 partition aanmaken)

#@app.agent(topic)
#async def ipflow(ipflows):
#    async for flow in ipflows:
#        print(f'Source IP address: {flow.src_ip}')

@app.agent(topic)
async def count_ipflows(ipflows):
    async for flow in ipflows:
        ## hier de table gebruiken als counter voor het aantal flows
        print(f'{flow} is flow nr {total_flows["total"]}') 


if __name__=="__main__":
    app.main()
