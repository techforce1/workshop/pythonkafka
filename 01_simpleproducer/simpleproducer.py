"""
Maak op basis van de example simple producer (00_example) een simpele producer die:
- hetzelfde statische bericht produceert produceert op topic Craftsmen
- het bericht zelf een string naar keuze
"""
import faust

# maak Faust app aan
app = faust.App('simpleproducer', broker='kafka-1:19092')
topic = app.topic(#vul in & aan)

# registreer een async func (send_message):
# produceer een string (value) elke (interval) seconden
@app.timer(interval=1.0)
async def send_message(message):
    ## voeg functie body toe 

if __name__ == '__main__':

    # Start de Faust async eventloop
    app.main()
