## maak zelf een 'simpleproducer' op basis van voorbeelden in 00_example met topic naam CodeFoundry 

- aanwijzingen in "skelet" producer simpleproducer.py
- start de producer vanuit een separaat terminal window/tab biunnen de folder zelf met:

$ python3 ./simpleproducer.py worker -l debug --witthout-web 

- om productie te controleren en meteen te verifieren dat topic CodeFoundry ook daadwerkelijk aangemaakt is:

$ kafkacat -L -b kafka-1:19092 
    - geeft metadata over Kafka cluster (bestaande topics, partitions, etc)

$ kafkacat -C -b kafka-1:19092 -t "CodeFoundry" 

    - consumed het aangemaakte topic *mits* die goed aangemaakt en er op geproduced wordt door 'simpleproducer.py' 