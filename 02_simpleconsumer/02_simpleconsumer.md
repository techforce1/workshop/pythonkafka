## maak zelf een 'simpleconsumer' op basis van voorbeelden in 00_example die alle berichten van vorige opdracht consumeert en weergeeft

- aanwijzingen in "skelet" producer simpleconsumer.py
- start de producer vanuit een separaat terminal window/tab biunnen de folder zelf met:

$ python3 ./simpleconsumer.py worker -l debug --witthout-web 

- om productie te controleren en meteen te verifieren dat topic Craftsmen ook daadwerkelijk aangemaakt is:

$ kafkacat -L -b kafka-1:19092 
    - geeft metadata over Kafka cluster (bestaande topics, partitions, etc)

$ kafkacat -C -b kafka-1:19092 -t "Craftsmen" 

    - consumed het aangemaakte topic *mits* die goed aangemaakt en er op geproduced wordt door 'simpleproducer.py' om te verifieren dat er ook daadwerkelijk berichten te consumeren zijn 